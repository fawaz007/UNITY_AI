﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Agentplayer : AI_a_Agent {

	public override void Update()
	{
		velocity.x = Input.GetAxis("Horizontal")*Time.deltaTime;
		velocity.z = Input.GetAxis("Vertical")*Time.deltaTime;
		velocity *= maxSpeed;
		Vector3 translation = velocity * Time.deltaTime;
		transform.Translate(translation, Space.World);
		transform.LookAt(transform.position + velocity);
		orientation = transform.rotation.eulerAngles.y;
	}
}
