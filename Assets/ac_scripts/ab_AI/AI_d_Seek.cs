﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_d_Seek : AI_c_AgentBehaviour
{
	public override AI_f_Steering GetSteering()
	{
		AI_f_Steering steering = new AI_f_Steering();
		steering.linear = target.transform.position - transform.
			position;
		steering.linear.Normalize();
		steering.linear = steering.linear * agent.maxAccel;
		return steering;
	}
}

