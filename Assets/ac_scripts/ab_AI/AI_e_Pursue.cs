﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_e_Pursue : AI_d_Seek {

	public float maxPrediction;
	private GameObject targetAux;
	private AI_a_Agent targetAgent;

	public override void Awake()
	{
		base.Awake();
		targetAgent = target.GetComponent<AI_a_Agent>();
		targetAux = target;
		target = new GameObject();
	}

	void OnDestroy ()
	{
		Destroy(targetAux);
	}

	public override AI_f_Steering GetSteering()
	{
		Vector3 direction = targetAux.transform.position - transform.
			position;
		float distance = direction.magnitude;
		float speed = agent.velocity.magnitude;
		float prediction;
		if (speed <= distance / maxPrediction)
			prediction = maxPrediction;
		else
			prediction = distance / speed;
		target.transform.position = targetAux.transform.position;
		target.transform.position += targetAgent.velocity *
			prediction;
		return base.GetSteering();
	}
}
