﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_g_Flee : AI_c_AgentBehaviour {
	public override AI_f_Steering GetSteering()
	{
		AI_f_Steering steering = new AI_f_Steering();
		steering.linear = transform.position - target.transform.
			position;
		steering.linear.Normalize();
		steering.linear = steering.linear * agent.maxAccel;
		return steering;
	}
}
