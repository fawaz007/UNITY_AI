﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_c_AgentBehaviour : MonoBehaviour {

	public GameObject target;
	protected AI_a_Agent agent;
	public virtual void Awake ()
	{
		agent = gameObject.GetComponent<AI_a_Agent>();
	}
	public virtual void Update ()
	{
		agent.SetSteering(GetSteering());
	}
	public virtual AI_f_Steering GetSteering ()
	{
		return new AI_f_Steering();
	}
}
